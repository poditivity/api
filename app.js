const express = require("express");

const app = express();

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

app.use(express.json());


app.post('/users', async (req, res) => {
  try {
    const users = await prisma.users.create({ data: req.body });
  res.json(users);
  } catch (error) {
    console.error('Error retrieving users', error);
    res.status(500).send('Internal Server Error');
  }
});

app.get('/users', async (req, res) => {
  try {
    const users = await prisma.users.findMany();
    res.json(users);
  } catch (error) {
    console.error('Error retrieving users', error);
    res.status(500).send('Internal Server Error');
  }
});

const port = process.env.PORT || "3000";

app.listen(port, () => {
  console.log(`Server Running at http://localhost:${port}`);
});